<?php get_header(); ?>

	<main class="formulario" id="fale-conosco">
		<div class="container interna">
			<h1 class="title"><span><?php the_title(); ?></span></h1>

			<p class="subtitulo">Telefones</p>
			<ul class="list">
				<li><span>SOMA - 0800.284.3288</span></li>
			</ul>

			<p class="subtitulo">Informe os seus dados</p>

			<?php echo do_shortcode('[contact-form-7 id="76" title="Fale Conosco"]') ?>
			<div id="mapa">
				<div class="row">
					<div class="col s12 m6 lg6">
						<p class="subtitulo">Localização</p>
						<p class="mapa-endereco">R. Eleutério Dom da Silva, 143 - Centro, Lauro de Freitas - BA</p>
					</div>
					<div class="col s12 m6 lg6">
						<!-- <div class="input-field">
							<select class="mapa-cidades">
									<option value="0">FORTALEZA - CE</option>
									<option value="1">TEREZINA - PI</option>
							</select>
						</div> -->
					</div>
				</div>
				<div>
					<iframe class="mapa-google" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15556.804422898906!2d-38.3302186!3d-12.8947864!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc00295ba0a68f274!2sSoma!5e0!3m2!1spt-BR!2sbr!4v1490992800233" width="970" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>
