<?php


// add scripts in page
function my_scripts() {
	// deregistering old jquery
	wp_deregister_script('jquery');

	// styles
	wp_register_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
    wp_enqueue_style('fontawesome' );

    wp_register_style('materialfont', 'http://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style('materialfont' );

	wp_register_style('materialize.css', get_template_directory_uri() . '/assets/css/lib/materialize.min.css');
    wp_enqueue_style('materialize.css' );


     wp_register_style('fresco', get_template_directory_uri() . '/assets/css/lib/fresco/fresco.css');
    wp_enqueue_style('fresco' );

    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style' );
    
    

    // Javascripts
	wp_register_script('jquery', get_stylesheet_directory_uri() . '/assets/js/lib/jquery-2.2.4.min.js', null, null, true);
	wp_enqueue_script('jquery');
	
	wp_register_script('materialize.js', get_stylesheet_directory_uri() . '/assets/js/lib/materialize.min.js', null, null, true);
	wp_enqueue_script('materialize.js');

   	wp_register_script('less', get_stylesheet_directory_uri() . '/assets/js/lib/less.js', null, null, true);
	wp_enqueue_script('less');

	wp_register_script('cycle2', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.cycle2.min.js', null, null, true);
	wp_enqueue_script('cycle2');

	wp_register_script('cycle2.carousel', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.cycle2.carousel.min.js', null, null, true);
	wp_enqueue_script('cycle2.carousel');

	wp_register_script('mask', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.mask.min.js', null, null, true);
	wp_enqueue_script('mask');

	wp_register_script('frescojs', get_stylesheet_directory_uri() . '/assets/js/lib/fresco.js', null, null, true);
	wp_enqueue_script('frescojs');

	// chamar o script
	wp_register_script('script', get_stylesheet_directory_uri() . '/assets/js/script.js', null, null, true);
	wp_enqueue_script('script');

	
	function get_excerpt($count){
		$permalink = get_permalink($post->ID);
		$excerpt = get_the_content();
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, $count);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = $excerpt.'...';
	return $excerpt;
}

	// Define a variável ajaxurl
    $script  = '<script>';
    $script .= 'var ajaxurl = "' . admin_url('admin-ajax.php') . '";';
    $script .= '</script>';
    echo $script;

}

add_action( 'wp_enqueue_scripts', 'my_scripts' );
add_filter('show_admin_bar', '__return_false');
add_theme_support( 'post-thumbnails' );
