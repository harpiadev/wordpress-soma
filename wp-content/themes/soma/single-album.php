<?php  get_header(); ?>

	<main id="album">
		<div class="container interna">
		 	<?php the_post(); ?>
			<h1 class="title"><span><?php the_title();?></span></h1>
			<?php 
			$images = get_field('imagens');
		 	if( $images ): ?> 
			 	<div class="row">
			 		<?php foreach( $images as $image ): ?>
						<div class="col s6 m3 imagem-album">
							<div class="ratio ratio-1-1">
								<div class="content">
									<a href="<?php echo $image['imagem']['sizes']['large']; ?>" class="fresco" data-fresco-group="galeria-album">
										<img src="<?php echo $image['imagem']['sizes']['medium_large']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							</div>
						</div>
					<?php endforeach; ?>					 		 	
			 	</div>
		 	<?php endif; ?>
		</div>
	</main>

<?php get_footer(); ?>