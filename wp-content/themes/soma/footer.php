	<footer class="footer" id="footer">
		<section id="main-footer">
				<div class="container">
					<h2 id="title-footer">Fique por dentro</h2>
					<p id="sub-title-footer">Fique  por dentro de tudo que acontece na nossa empresa. Nos acompanhe de perto.</p>
					<form  method="post" action="<?php echo home_url('/?na=s');?>" >
						<div class="input-field">
							<input type="email" name="ne" required placeholder="Informe seu e-mail...">
							<button type="submit" class="waves-effect btn waves-light color-light">INSCREVA-SE</button>
						</div>
					</form>
				</div>
		</section>
		<section id="copy-right">
			<div class="container">
				<div class="row">
					<div class="col s12 m6">
						<p class="copyrights-text">Todos os direitos reservados a <span>Emmarka LTDA</span></p>
					</div>
					<div class="col s12 m6">
						<p class="assign">Desenvolvido por: <a href="http://harpiadev.com.br" target="blank"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-harpia-cinza.png" alt="Harpia Soluções em Tecnologia"></a>
						</p>
					</div>
				</div>
			</div>
		</section>
	</footer>

	<?php wp_footer(); ?>
	
</body>
</html>
